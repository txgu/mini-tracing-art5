# MiniTrace

This is a project for mini-tracing based on `refs/tags/android-5.1.1_r3`.

Now we support fuzzing actions.

## Build

1. Checkout the all source code of Android. Note that we currently require `5.1.1_r3`.
2. Follow the guidelines in [Downloading and Building](https://source.android.com/source/requirements.html) section.

        source build/envsetup.sh
        launch aosp_arm-eng
        hmm
   `hmm` will print a help message on how to build a single module in the source tree.

        Invoke ". build/envsetup.sh" from your shell to add the following functions to your environment:
        - lunch:   lunch <product_name>-<build_variant>
        - tapas:   tapas [<App1> <App2> ...] [arm|x86|mips|armv5|arm64|x86_64|mips64] [eng|userdebug|user]
        - croot:   Changes directory to the top of the tree.
        - m:       Makes from the top of the tree.
        - mm:      Builds all of the modules in the current directory, but not their dependencies.
        - mmm:     Builds all of the modules in the supplied directories, but not their dependencies.
                   To limit the modules being built use the syntax: mmm dir/:target1,target2.
        - mma:     Builds all of the modules in the current directory, and their dependencies.
        - mmma:    Builds all of the modules in the supplied directories, and their dependencies.
        - cgrep:   Greps on all local C/C++ files.
        - ggrep:   Greps on all local Gradle files.
        - jgrep:   Greps on all local Java files.
        - resgrep: Greps on all local res/*.xml files.
        - sgrep:   Greps on all local source files.
        - godir:   Go to the directory containing a file.
        
        Look at the source to view more functions. The complete list is:
        adb_get_product_device adb_get_traced_by addcompletions add_lunch_combo cgrep check_product check_variant choosecombo chooseproduct choosetype choosevariant core coredump_enable coredump_setup cproj croot findmakefile gdbclient gdbclient_old gdbwrapper get_abs_build_var getbugreports get_build_var getdriver getlastscreenshot get_make_command getprebuilt getscreenshotpath getsdcardpath get_symbols_directory gettargetarch gettop ggrep godir hmm is isviewserverstarted jgrep key_back key_home key_menu lunch _lunch m make mangrep mgrep mm mma mmm mmma pez pid printconfig print_lunch_menu qpid resgrep runhat runtest sepgrep set_java_home setpaths set_sequence_number set_stuff_for_environment settitle sgrep smoketest stacks startviewserver stopviewserver systemstack tapas tracedmdump treegrep

    Once you have built all dependencies, you can build `libart.so` by invoke `make libart` in the top directory.
3. As we want to build art, we can change the directory into the art and invoke `mma`.
4. Then, you will find a command `art` in your path, type `which art` to check it. This is a x86 version in your host machine, e.g., ubuntu 14.04 for me.
5. Next prepare a `dex` file to test art. Refer to [this](http://milk.com/kodebase/dalvik-docs-mirror/docs/hello-world.html) to learn how to prepare a `dex` file.
6. As now we already have the `art` script to setup all environment variables, we can simple invoke art via `art -cp classes.dex HelloWorld`.

## Install

You should root your phone in order to replace the `/system/lib/libart.so` with your custom build.
Currently, the custom `libart.so` can only be installed on a Nexus 5 with **Android 5.1.1\_r3**.
Other platforms are not tested, but the code we added is quite small, only 170 lines.
You can download the Android Open Source Project for your phone and then apply the patch to the `art`.


## Usage


1. Make sure `adb` is in your `PATH`.
2. Log into your phone via `adb shell`, and switch to root via `su`.
3. Launch an apps in your phone. You could start a process via `adb` in command line.
4. Get the pid of your phone via `ps | grep <your app>`
5. At first, MiniTrace is toggled off. Turn it on via `kill -USR2 $PID`.
   You can also start the MiniTrace via a configuration file. See below.
6. Run your test script.
7. Once the test has finished, turn off the MiniTrace via `kill -USR2 $PID`.
   The trace can only be written to the file at `/sdcard/mini_tracing.log` if your have manually toggle off MiniTrace.

## Configuration and Log File

There are three files

1. `/sdcard/mini_trace_$uid_config.in`: configuration
2. `/sdcard/mini_trace_$uid_info.log`: visited methods and fields, and coverage information
3. `/sdcard/mini_trace_$uid_data.bin`: binary trace data

A parser for these files can be found at [minitrace in android-toolkit](http://lab.artemisprojects.org/tianxiaogu/android-toolkit/tree/master/minitrace/minitrace)

### Traced Events

We only plan to implement tracing in the interpreter.
We only force methods in the apps to be interpreted and we only guarantee to trace those events.
Other methods of the framework are not guaranteed to be traced.


1. Method Enter/Exit
2. Instance Field Get/Put
3. Static Field Get/Put
4. ~~Monitor Enter/Exit~~
5. ~~Array Element Load/Store~~
6. Coverage

### Configuration

As all apps are started by `ActivityManagerService` and forked from the zygote,
we cannot specify startup options for the ART.
Therefore, we have to read options in another way.

Currently, we support two ways to trigger MiniTrace

1. Use `SIGUSR2` when we can get the pid.
2. Use a configuration file at `/sdcard/mini_trace_$uid_config.in`

MiniTrace will first check whether the file exists.

#### Apps without `SDCARD_RW` Permission

Many apps have no permission to read and write SDCARD.
Therefore, we also support put the same configure file at `/data`.
Besides, some priority should be set.

1. Touch three files for config, info and data for mini trace 
    1. `/data/mini_trace_$uid_config.in`
    2. `/data/mini_trace_$uid_info.log`
    3. `/data/mini_trace_$uid_data.bin`
2. Set the owner of these three files to the apps-under-test by `chown`. For example, the `Calculator` has a user name `u0_a31`.
   This can be checked the owner for directory `/data/data/<package-name>/cache`.
3. Set selinux to permissive mode

        adb shell su -c setenforce 0


#### Configuration File Format

One line an options.

Supported:

1. `off`
2. `MethodEvent`
3. `FieldEvent`
4. `MonitorEvent` (Not implement yet!)
6. `DoCoverage`

If the file is empty, then all available options are opened.



### Fuzzing

An example of configuration file is shown here:

    verbose 0
    throw 30
    Lcom/android/okio/RealBufferedSource$1; read ()I 1000000 java/io/IOException 0 
    Lcom/android/okio/RealBufferedSource$1; read ([BII)I 2000000 java/io/IOException 0 


Note the trailing white space.



